---
title: Promise methods in JavaScript
date: 2023-03-14 12:55:37
tags:
---

Let's learn about Promise methods in JavaScript.

## Promise.all()

`Promise.all()` is a method that takes an iterable(like an array) of promises and returns a single `Promise` that resolves when all of the promises inside the input resolves or rejects as soon as one of the promises in the iterable input rejects. The single promise that is returned, resolves with an array of all the resolved values of promises inside the input or it rejects with the reason of the first promise which rejects.

Let's see with code:

```js
const promise1 = 10;
const promise2 = Promise.resolve(20);
const promise3 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(30);
    }, 2 * 1000);

Promise.all([promise1, promise2, promise3])
.then((resolveValues) => {
    console.log(resolveValues);
});

})
```

Output:

```js
[ 10, 20, 30 ]
```

The output comes after 2 seconds this is because, **`Promise.all()` will wait for all promises to resolve, then it will return a new `promise`.**

We can see that the output is in the form of array and the order of the resolved values is according to the order of the promises passed.

This method can be useful when you have more than one promise, and you want to know when all of the promises have resolved.

**So `Promise.all()` waits for all promises to succeed and fails immediately if any of the promises in the array fails.**

## Promise.allSettled()

The `Promise.allSettled()` static method takes an iterable(like an array) of promises as input and returns a **single** `Promise`. This returned `promise` fulfills when all of the `input's promises` settle i.e. resolves or rejects.

This is similar to `Promise.all()` except that it does not reject as soon as anyone to them rejects. It waits for all promises to resolve or reject and returns a `promise` which is always resolved with an array of objects. Each object constains two properties-

1. `status` with a value indicating `fulfilled` or `rejected`
2. `value` OR `reason`
   - `value` with value of the resolved promise OR

   - `reason` with reason if the promise is rejected.

Let's see how it works through code:

```js
const promise1 = 10;
const promise2 = Promise.resolve(10);
const promise3 = new Promise((resolve, reject) =>{
    setTimeout(() => {
    reject("failed");
    }, 2 * 1000);
});

const promises = [promise1, promise2, promise3];

Promise.allSettled(promises).
  then((results) => console.log(results));

```

Output:

```js
[
  { status: 'fulfilled', value: 10 },
  { status: 'fulfilled', value: 10 },
  { status: 'rejected', reason: 'failed' }
]
```

**`Promise.allSettled()` is typically used when you have multiple asynchronous tasks that are not dependent on one another to complete successfully, or you'd always like to know the result of each promise.**

**In comparison, the Promise returned by `Promise.all()` may be more appropriate if the tasks are dependent on each other, or if you'd like to immediately reject upon any of them rejecting.**

## Promise.any()

It takes an iterable of promises as its argument and returns a new `promise` that resolves as soon as any of the promises in the iterable have resolved. It resolves with the value of the first fulfilled `Promise`. If all of the promises in the iterable are rejected, then `Promise.any()` will reject with an AggregateError that contains the rejection reasons of all the promises.

Here's an example if all promises reject:

```js
const promise1 = Promise.reject(10);
const promise2 = new Promise((resolve, reject) =>{
    setTimeout(() => {
    reject("failed");
    }, 2 * 1000);
});

const promises = [promise1, promise2];

Promise.any(promises).
  then((results) => {
    console.log(results);
  }).
  catch((errors) => {
    console.log(errors);
  });
```

Output:

```js
[AggregateError: All promises were rejected] {
  [errors]: [ 10, 'failed' ]
}
```

Another example if any one promise is resolved:

```js
const promise1 = Promise.reject("failed 1");
const promise2 = new Promise((resolve, reject) =>{
    setTimeout(() => {
    resolve("passed 2");
    }, 2 * 1000);
});

const promise3 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("passed 3");
    }, 1 * 1000)
});

const promises = [promise1, promise2, promise3];

Promise.any(promises).
  then((results) => {
    console.log(results);
    }).
  catch((errors) => {
    console.log(errors);
  });
```

Output:

```js
passed 3
```

We can see from the above output that as soon as `promise3` is resolved, `Promise.any()` also resolved with the value of the first promise resolved i.e. `promise3`.

**Therefore, `Promise.any()` will only wait for the one promise to fulfill and it quickly returns a promise which resolves with the fulfillment value of the first promise that is resolved.**

## Promise.race()

The `Promise.race()` method takes an iterable of promises as input and returns a single `Promise`. This returned promise settles with the eventual state of the first promise that settles.

This method is very similar to `Promise.any()`, but it only waits until one promise either **fails** or **succeeds** unlike `Promise.any` which only cares about the first promise which is either fulfilled or rejected. `Promise.race` will wait until the first promise fails or succeeds and will then resolve or reject with the first promise that settles.

Let's see it in practical:

```js
const promise1 = new Promise((resolve, reject) =>{
    setTimeout(() => {
        resolve("passed 1");
    }, 2 * 1000);
});

const promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        reject("failed 2");
    }, 1 * 1000)
});

const promises = [promise1, promise2];

Promise.race(promises).
  then((results) => {
    console.log(results);
    }).
  catch((errors) => {
    console.log(errors);
  });
```

Output:

```js
failed 2
```

`Promise.race()` waits until any one promise settles and returns a `promise` which resolves or rejects according to the first promise which settles.

**So, `Promise.race` returns a promise as soon as any one promise from the iterable input settles. The returned promise is either resolved or rejected based on the fulfillment or rejection of the first promise to settle.**

## Summary

- `Promise.all()` waits for all promises to succeed or fails immediately if any of the promises in the input iterable fails. It returns a single `promise` which resolves if all promises succeed or rejects if any one fails.

- `Promise.allSettled` waits for all promises to settle(resolve or reject) and returns a `promise` which is always resolved with an array of objects with settle status of promises.

- `Promise.any()` will only wait for the one `promise` to fulfill and returns a `promise` which resolves with the fulfillment value of the first promise that is resolved or rejects with an AggregateError that contains the rejection reasons of all the promises.

- `Promise.race` returns a `promise` as soon as any one promise from the iterable input settles. The returned promise is either resolved or rejected based on the fulfillment or rejection of the first promise to settle.
