---
title: Truthy and Falsy values in Javascript
date: 2023-02-22 11:10:49
tags:
---

Every value in Javascript has a type but each value also has an inherent boolean value known as **truthy** and **falsy** values which determines how a value is translated into boolean context. **Truthy** and **Falsy** are values in Javascript which is considered to be **true** and **false** respectively in boolean context. JavaScript uses type conversion to coerce any value to a Boolean in contexts that require it, such as conditionals and loops. Therefore any single value can be used within conditions. For example,

```js
if (value) {
  // if value is truthy, if block executes
}
else {
  // if value is falsy, else block executes
}
```

Let's start with falsy values because in Javascript **everything is considered to be truthy except the falsy values.**

### Falsy Values

A falsy (sometimes written falsey) value is a value that is considered false when encountered in a Boolean context. There are only few falsy values. Here is a complete list of falsy values in javascript.

| Value                         | Description                              |
|-------                        | -----------------                        |
| ```false```                   | The false keyword                        |
| ```0```                       | The number zero (also 0.0, etc., and 0*0)|
| ```-0```                      | Negative zero (also -0.0, etc., and -0*0 |
| ```0n```                      | The BigInt zero (so, also 0x0n). Note that there is no BigInt negative zero — the negation of 0n is 0n. |
| ```""```, ```''```, ``` `` ```| Empty string                             |
| null                          | null- the absence of any value           |
| undefined                     | undefined- The primitive value           |
| NaN                           | NaN- Not a Number                        |

#### Examples to falsy values

Examples of falsy values in JavaScript (which are coerced to false in Boolean contexts, and thus bypass the if block):

```javascript
if(false){
    //not reachable
}

if(0){
    //not reachable
}
if(-0){
    //not reachable
}

if(null){
    //not reachable
}
if(undefined){
    //not reachable
}
if(NaN){
    //not reachable
}
```

### Truthy Values

As already mentioned, A **truthy value** is a value which is considered to be true in boolean context.
All values in Javascript is considered to be truthy **except the above mentioned falsy values.**

Here are some examples to truthy values:

```js
if(true)
if(1)
if({})
if([])
if("string")
if("0")
if("false")
if(Infinity)
if(-Infinity)
if(-10)
if(1.41)

```

In these examples the if block will execute as the values inside the if condition is truthy. If we notice the values like ```"0"``` or ```"false"``` in our example, is considered truthy. This is because they are strings and we already know all values except the falsy ones, are truthy.

## Conclusion

Remember that every value in javascript can be translated into it's boolean context and all values are considered to be truthy except only the falsy values.
