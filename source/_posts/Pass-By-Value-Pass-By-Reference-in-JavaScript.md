---
title: Pass By Value and Pass By Reference in JavaScript
date: 2023-03-02 12:26:30
tags:
---

In this post, we will look into pass by value and pass by reference in Javascript.

**JavaScript is always pass by value.** This means everything in JavaScript is value type and arguments passed to functions are pass by value.

Lets first see, what excatly is Pass By Value and Pass By Reference.

## Pass-By-Value

In Pass by value, function is called by directly passing the value of the variable as an argument. So any changes made inside the function to that variable does not affect the original value. Let's see with example:

```JS
function passByValue(element){
    element = 10;
    console.log("Element Inside Function: "+ element);
}

let element = 1;

passByValue(element);

console.log("Original element: " + element);
```

Output of the above code:

```js
element Inside Function: 10
Original element: 1
```

Here we can see changing `element` (argument) inside function does not change the original `element`

Now let's see what is pass by reference.

## Pass-By-Reference

In Pass by Reference, Function is called by directly passing the reference/address of the variable as an argument. So changing the value inside the function also change the original value.
Let's see an example:

```JS

function passbyReference(obj) {
    obj.key = 20;
}

let obj = {
    key : 10
}
console.log("Before Function Call: " + obj.key);

passbyReference(obj);

console.log("After Function Call: " + obj.key);
```

Output of the above code:

``` JS
Before Function Call: 10
After Function Call: 20
```

We can see that the object's key is changed after passing it to the function.

After seeing the result, question arises that if the original `obj` passed as argument is modified inside the function then how come JavaScript is **always pass by value.**

Yes, the object types in JavaScript are a bit more confusing. The confusion lies in the fact that object types are reference types which are passed by value. As weird as this sounds, a reference to an object is passed to a function by value. The subtle difference here lies in the fact that an object reference passed by value is not the same as passing an object by reference.

Simply put, changes to the object inside the function will affect the original object, as they both refer to the same object. However, reassigning the value of the variable holding the object originally will not affect the object referenced by the function. Let me put this with an example:

```JS
function passbyReference(obj) {
    obj = {
        key : 20
    }
    return obj;
}

let obj = {
    key : 10
}

let newObj = passbyReference(obj);
console.log("Original obj " + obj.key);

console.log("After Function Call: " + newObj.key);
```

Output of above code:

``` JS
Original obj: 10
After Function Call: 20
```

We can see from the above code that even after reassigning the `obj` inside the function does not change the original `obj`. This is because original `obj` was **not passed by reference**, instead the reference of that `obj` was passed by value to the function.

>**Note: When we reassign the object inside function, we are not actually reassigning the object. We just reassign the reference to that object.**

**So if we are passing object or array as an argument to the method, then there is a possibility that value of the object can change and also a possibility that value of the object might not change (in case of reassignment).**

### But what if we want to add an element to an array or add a key to an object without modifying the original array or object?

Yes, we can do this by using spread operator.

### What is spread operator?

The spread (`...`) syntax allows an iterable, such as an array or string, to be expanded in places where zero or more arguments (for function calls) or elements (for array literals) are expected. In an object literal, the spread syntax enumerates the properties of an object and adds the key-value pairs to the object being created.

Let's see an example to better understand this.

```js
function sum(num1, num2, num3, num4){
    return num1 + num2 + num3 + num4;
}

//Suppose we have an array of number
let nums = [2,5,9,10];

//Passing array elements to the sum function but using spread operator.
let arrSum = sum(...nums);
console.log("sum = " + arrSum);
```

**Output:**

`sum = 26`

By seeing the above code we can get an idea about what spread operator does. It expands the iterable (like array or string) to individual elements.

Now, coming back to our topic to modify array or object without changing the original array or object.

Let's see it in practical

- For arrays : Suppose we need to add an element to the array `nums` but without changing it.

```js
function spreadArray(nums){
    //Reassigns nums to a different array
    nums = [...nums, 20];
    return nums;
}

let nums = [2,5,9,10];
let newNums = spreadArray(nums);

console.log("Original nums: " + nums);
console.log("Modified nums: " + newNums);
```

**Output:**

```js
Original nums: 2,5,9,10
Modified nums: 2,5,9,10,20
```

We can see by this example that original array is not mutated rather `nums` inside the function `spreadArray` is assigned to a new array created using spread operator.

- Let's see for objects : Suppose if we want to add a new key or modify the current key without mutating original object.

```js
function spreadObject(user){
    let newUser = {...user, age : 25, city : 'New York'};
    return newUser;
}

let user = {
    name : "John",
    age : 20
};

let newUser = spreadObject(user);

console.log("Original user : " + JSON.stringify(user));
console.log("Modified user : " + JSON.stringify(newUser));
```

**Output:**

```js
Original user : {"name":"John","age":20}
Modified user : {"name":"John","age":25,"city":"New York"}
```

Changes are not reflected, as we can see in both cases.

So, Now we know that if we don't want to reflect changes in original array or object, we can use spread operator.

In simple words- **Spread operator does nothing but iterates over the iterable data structure and expands it in individual values. Therefore, we are actually creating a new array or object using the original one.**

## Conclusion

- JavaScript is **always Pass-By-Value.**
- Reassigning an object, passed as argument inside function does not affect the original object.
- We can add/change the element or property of arrays or object without mutating the original using the **spread (`...`) operator.**
