---
title: Sort in JavaScript
date: 2023-03-06 14:49:13
tags:
---

In this post we will look how to use JavaScript built-in `sort()` method. The `sort()` method sorts the elements of an array in place and returns the reference to the same array, now sorted. The default sort order is ascending, built upon converting the elements into strings, then comparing their sequences of UTF-16 code units values.

Let's see how we can use sort on different data types:

## Strings

Let's say we have an array of strings, like so:

```js
["John", "Zuck", "Ben", "Alan", "Sam"];
```

Now we will sort this array using default `sort()` method.

```js
let names = ["john", "zuck", "ben", "alan", "sam"];
let sortedNames = names.sort();
console.log(names);
```

**Output:**

```js
[ 'alan', 'ben', 'john', 'sam', 'zuck' ]
```

> Note: Not to be confused with the return value of the `sort()` method. **The `sort()` method actually mutates the original array and both the original and sorted arrays are the same references.**

```js
console.log(names === sortedNames); // returns true
```

**Output:**

`true`

This is because `sortedNames` and `Names` refer to the same array.

- The default `sort()` method sorts the array in ascending order.

What if we want to sort in decending order.

We can do this by `compare function`. The `sort()` function also accepts a `compare function` as a callback.
Let's see by an example:

```js
let names = ["john", "zuck", "ben", "alan", "sam"];
let sortedNames = names.sort(function compareFunction(fName, sName){
    if(fName > sName){
        return -1;
    }
    else if(fName < sName){
        return 1;
    }
    else{
        return 0;
    }
});
console.log(names);
```

**Output:**

```js
[ 'zuck', 'sam', 'john', 'ben', 'alan' ]
```

**What about different cases in String**, Let's check it out by an example:

```js
let names = ["john", "Zuck", "ben", "alan", "sam"];
names.sort();
console.log(names);
```

**Output:**

```js
[ 'Zuck', 'alan', 'ben', 'john', 'sam' ]
```

Oops, how `Zuck` came before every other element.

This is because in JavaScript, **uppercase letters come before lowercase letters in UTF-16 encoding and JavaScipt compares characters by their UTF-16 values.**

What if we want to ignore case while sorting. Yes, we can also do this by using a `compare function`-

```js
let names = ["john", "Zuck", "ben", "alan", "sam"];
names.sort((fName, sName) => {
    fName = fName.toLowerCase();
    sName = sName.toLowerCase();
    if(fName < sName){
        return -1;
    }
    else if(fName < sName){
        return 1;
    }
    else{
        return 0;
    }
});
console.log(names);
```

Now, we pretty much know about sorting array of strings. What about Integers, let's see-

## Integers

The same philosophy applies. The default sort() function can not do it correctly. See this,

```js
let numbers = [2, 1000, 10, 3, 23, 12, 30, 21];

numbers.sort();
console.log(numbers);
```

**Output:**

```js
[10, 1000, 12, 2, 21, 23, 3, 30]
```

Weird right, Yeah because with the default sort(), elements are converted to strings and compared in UTF-16 code units order. Hence the converted "12" comes before the converted "2".

We need a compare function again. An ascending order sort can be done as,

```js
let numbers = [2, 1000, 10, 3, 23, 12, 30, 21];

numbers.sort((num1, num2) => {
    return num1 - num2;
    });

console.log(numbers);
```

**Output:**

```js
[2, 3, 10, 12, 21, 23, 30, 1000]
```

Similarly, we can sort in descending order just by changing the return statement, Instead of returning `num1 - num2`, return `num2 - num1` in compare function.

```js
numbers.sort((num1, num2) => {
    return num2 - num1;
    });
console.log(numbers);
```

**Output:**

```js
[1000, 30, 23, 21, 12, 10, 3, 2]
```

What about floating point number?

## Floating Point Numbers

Sorting floating point numbers is pretty much similar to sorting integers.

Lets take two examples- One for ascending and one for descending.

- Ascending

```js
let numbers = [82.11, 28.86, 49.61, 5.86, 17.57];
numbers.sort((num1, num2) => {
    return num1 - num2;
});
console.log(numbers);
```

**Output:**

```js
[ 5.86, 17.57, 28.86, 49.61, 82.11 ]
```

- Descending

```js
let numbers = [82.11, 28.86, 49.61, 5.86, 17.57];
numbers.sort((num1, num2) => {
    return num2 - num1;
});
console.log(numbers);
```

**Output:**

```js
[ 82.11, 49.61, 28.86, 17.57, 5.86 ]
```

We, can see both integers and floats follow same pattern for sorting.

Let's move to objects now.

## Objects

In JavaScript, `objects` are used to store multiple values as a complex data structure.

An object is created with curly braces `{…}` and a list of `properties`. A `property` is a `key-value` pair where the key must be a string and the `value` can be of any type.

Sorting an `object` is mostly about sorting based on property values. As the values can be of any type, let us understand various sorting with examples,

```js
let users = [
    {
        'firstName' : 'Shubham', 
        'lastName' : 'Wadkar',
        'address' : 'Maharashtra', 
        'age' : '26'
    },
    {
        'firstName' : 'Sunil', 
        'lastName' : 'Kumar',
        'address' : 'Odisha', 
        'age' : '25'
    },
    {
        'firstName' : 'Ranjeet',
        'lastName' : 'Kumar',
        'address': 'Delhi', 
        'age':'23'
    },
    {
        'firstName': 'Bhabesh', 
        'lastName' : 'Mahato',
        'address': 'West Bengal', 
        'age':'24'
    }
];
```

---

### Sorting by address

```js
users.sort((user1, user2) => {
    let address1 = user1.address;
    let address2 = user2.address;
    return address1 === address2 ? 0 : address1 > address2 ? 1 : -1;
});

console.table(users);
```

**Output:**

|   index  |   firstName   |    lastName    | address     |   age    |
|----      |----           |----            |----         |----      |
|0         |Ranjeet        |    Kumar       |Delhi        |23        |
|1         |Shubham        |    Wadkar      |Maharashtra  |26        |
|2         |Sunil          |    Kumar       |Odisha       |25        |
|3         |Bhabesh        |    Mahato      |West Bengal  |24        |

---

### Sorting by age

```js
users.sort((user1, user2) => {
    let age1 = user1.age;
    let age2 = user2.age;
    return age1 === age2 ? 0 : age1 > age2 ? 1 : -1;
});

console.table(users);
```

**Output:**

|   index  |   firstName   |    lastName    | address     |   age    |
|----      |----           |----            |----         |----      |
|0         |Ranjeet        |    Kumar       |Delhi        |23        |
|1         |Bhabesh        |    Mahato      |West Bengal  |24        |
|2         |Sunil          |    Kumar       |Odisha       |25        |
|3         |Shubham        |    Wadkar      |Maharashtra  |26        |

---

**Sorting by Multiple Keys:**

What if we find similar values in two different objects?
Then we need another criteria to sort, let's understand by example,

Suppose we need to sort by `lastName`, but if we find two `lastName`s equal then we will compare their `firstName`.

### Sorting by name

```js
users.sort((user1, user2) => {
    let lastName1 = user1.lastName;
    let lastName2 = user2.lastName;
    let firstName1 = user1.firstName;
    let firstName2 = user2.firstName;
    if(lastName1 > lastName2){
        return 1;
    }
    else if(lastName1 < lastName2){
        return -1;
    }
    else {
        if(firstName1 > firstName2){
            return 1;
        }
        else if(firstName1 < firstName2){
            return -1;
        }
        else {
            return 0;
        }
    }
});

console.table(users);
```

**Output:**

|   index  |   firstName   |    lastName    | address     |   age    |
|----      |----           |----            |----         |----      |
|0         |Ranjeet        |    Kumar       |Delhi        |23        |
|1         |Sunil          |    Kumar       |Odisha       |25        |
|2         |Bhabesh        |    Mahato      |West Bengal  |24        |
|3         |Shubham        |    Wadkar      |Maharashtra  |26        |

## Summary

- The default `sort()` method sorts strings in ascending order.
- `sort()` method takes a callback `compare function` as an argument.
- Order of sorting depends upon the `compare function`.
- Integers can't be sorted using default `sort()`, needs a `compare function`.
- `sort()` behaves same for Integers and Floating point numbers.
- `Objects` can be sorted by `value` of the `key`, and we can also sort using multiple keys.
